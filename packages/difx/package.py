# Copyright 2013-2021 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack import *
import os


class Difx(Package):
    """Difx. correlates."""

    homepage = "https://www.atnf.csiro.au/vlbi/dokuwiki/doku.php/difx/start"
    svn      = "https://svn.atnf.csiro.au/difx/master_tags/"

    maintainers = ['patrick.yates']

    version('2.6.1', svn='https://svn.atnf.csiro.au/difx/master_tags/DiFX-2.6.1')
    version('2.6.2', svn='https://svn.atnf.csiro.au/difx/master_tags/DiFX-2.6.2')

    variant(
        'mk5daemon', default=False, description='Builds mk5daemon'
    )
    variant(
        'difxmonitor', default=False, description='Builds difx_monitor'
    )
    variant(
        'filterbank', default=False, description='Builds filterbank'
    )
    variant(
        'guiserver', default=False, description='Builds guiServer'
    )
    variant(
        'hops', default=False, description='Builds HOPS'
    )
    variant(
        'mark6support', default=False, description='Builds with Mark6 support'
    )
    variant(
        'mark6meta', default=False, description='Builds with Mark6 metadata support'
    )
    variant(
        'polconvert', default=False, description='Builds PolConvert'
    )
    variant(
        'datasim', default=False, description='Builds datasim application'
    )
    variant(
        'documentation', default=False, description='Builds mpifxcorr documentation'
    )

    # automake
    depends_on('m4')
    depends_on('autoconf@2.69')
    depends_on('automake')
    depends_on('libtool')
    depends_on('pkgconf')

    # docs
    depends_on('doxygen', when='+documentation')

    # parser generators
    depends_on('bison')
    depends_on('flex@2.5.4a-10')

    # compiler
    # depends_on('%gcc')

    # actual libraries
    depends_on('python@3.6:')
    depends_on('fftw-api@3')
    #depends_on('intel-ipp@2017.3.196:')
    depends_on('intel-oneapi-ipp@2021.12.0:')
    depends_on('mpi')
    depends_on('perl')
    depends_on('ntirpc')

    depends_on('pgplot')
    depends_on('gsl')
    depends_on('casacore', when='+polconvert')
    depends_on('expat')

    patch('genipppc-oneapi.patch')
    patch('genipppc-oneapi2.patch')

    def setup_build_environment(self, build_env):
        self._prep_difx_build_env(build_env, self.spec, self.prefix)
        self._prep_difx_run_env(build_env, self.spec, self.prefix)

    def setup_run_environment(self, run_env):
        self._prep_difx_run_env(run_env, self.spec, self.prefix)

    # def setup_environment(self, spack_env, run_env):
    #     self._prep_difx_build_env(spack_env, self.spec, self.prefix)
    #     self._prep_difx_run_env(run_env, self.spec, self.prefix)

    def setup_dependent_build_environment(self, build_env, dependent_spec):
        self._prep_difx_build_env(build_env, self.spec, self.prefix)

    def _prep_difx_build_env(self, env, spec, prefix):
        env.set('USEGFORTRAN', "yes")
        env.append_flags('FFLAGS', '-ffree-line-length-512')
        if spec.satisfies('%gcc@10:'):
            env.append_flags('FFLAGS', '-fallow-argument-mismatch')

    def _prep_difx_run_env(self, env, spec, prefix):
        vers = "Difx-{}".format(spec['difx'].version)
        env.set('DIFX_VERSION', vers)
        env.set('DIFXROOT',  prefix)
        env.set('DIFX_PREFIX', prefix)
        env.set('PGPLOTDIR', spec['pgplot'].prefix)
        if "^intel-oneapi-ipp" in spec:
            ipproot = spec['ipp'].package.component_prefix
        else:
            ipproot = spec['ipp'].prefix
        env.set('IPPROOT', ipproot)

        env.set("DIFX_MESSAGE_GROUP", "224.2.2.1")
        env.set("DIFX_MESSAGE_PORT", "50201")
        env.set("DIFX_BINARY_GROUP", "224.2.2.1")
        env.set("DIFX_BINARY_PORT", "50202")
        env.set("CALC_SERVER", "localhost")

        env.prepend_path('PATH', prefix.bin)
        # env.append_flags('PS1', vers)

        env.prepend_path('LD_LIBRARY_PATH', prefix.lib)
        env.prepend_path('LD_LIBRARY_PATH', prefix.lib64)

        env.prepend_path('PKG_CONFIG_PATH', prefix.lib.pkgconfig)
        env.prepend_path('PKG_CONFIG_PATH', prefix.lib64.pkgconfig)

        env.prepend_path('PYTHONPATH', prefix.lib.python)
        env.prepend_path('PYTHONPATH', prefix.lib64.python)

    def install(self, spec, prefix):
        install_difx = Executable('./install-difx')
        extra_args = []
        extra_args.append('--makeflags=-j') # make it fast

        if '+mk5daemon' in self.spec:
            extra_args.append('--mk5daemon')
        if '+difxmonitor' in self.spec:
            extra_args.append('--withmonitor')
        if '+filterbank' in self.spec:
            extra_args.append('--withfb')
        if '+guiserver' in self.spec:
            extra_args.append('--withguiserver')
        if '+hops' in self.spec:
            extra_args.append('--withhops')
        if '+mark6support' in self.spec:
            extra_args.append('--withm6support')
        if '+mark6meta' in self.spec:
            extra_args.append('--withmark6meta')
        if '+polconvert' in self.spec:
            extra_args.append('--withpolconvert')
        if '+datasim' in self.spec:
            extra_args.append('--withdatasim')
        if '+documentation' not in self.spec:
            extra_args.append('--nodoc')
        # install_difx('--makeflags=-j', '--nodoc')

        install_difx(*extra_args)
