# Copyright 2013-2021 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack import *
import os
import re
import llnl.util.filesystem as fs
import itertools


class Flex(AutotoolsPackage):
    """Flex is a tool for generating scanners."""

    homepage = "https://github.com/westes/flex"
    url = "https://github.com/westes/flex/releases/download/v2.6.1/flex-2.6.1.tar.gz"

    tags = ['build-tools']

    executables = ['^flex$']

    version('2.6.4', sha256='e87aae032bf07c26f85ac0ed3250998c37621d95f8bd748b31f15b33c45ee995')
    version('2.6.3', sha256='68b2742233e747c462f781462a2a1e299dc6207401dac8f0bbb316f48565c2aa')
    # Avoid flex '2.6.2' (major bug)
    # See issue #2554; https://github.com/westes/flex/issues/113
    version('2.6.1', sha256='3c43f9e658e45e8aae3cf69fa11803d60550865f023852830d557c5f0623c13b')
    version('2.6.0', sha256='cde6e46064a941a3810f7bbc612a2c39cb3aa29ce7eb775089c2515d0adfa7e9')
    version('2.5.39', sha256='258d3c9c38cae05932fb470db58b6a288a361c448399e6bda2694ef72a76e7cd')
    version('2.5.4a-10', sha256='791e8e26d35faed05654b17fe3a9e64eea0874d623c5aaaaf532e164378e8407', url='http://deb.debian.org/debian/pool/main/f/flex-old/flex-old_2.5.4a.orig.tar.gz')

    variant('lex', default=True,
            description="Provide symlinks for lex and libl")

    depends_on('bison',         type='build')
    depends_on('gettext@0.19:', type='build')
    depends_on('help2man',      type='build')
    depends_on('findutils',     type='build')
    depends_on('diffutils',     type='build')

    # Older tarballs don't come with a configure script and the patch for
    # 2.6.4 touches configure
    depends_on('m4',       type='build')
    depends_on('autoconf', type='build', when='@:2.6.0,2.6.4')
    depends_on('automake', type='build', when='@:2.6.0,2.6.4')
    depends_on('libtool',  type='build', when='@:2.6.0,2.6.4')

    # 2.6.4 fails to compile with newer versions of gcc/glibc, see:
    # - https://github.com/spack/spack/issues/8152
    # - https://github.com/spack/spack/issues/6942
    # - https://github.com/westes/flex/issues/241
    patch('https://github.com/westes/flex/commit/24fd0551333e7eded87b64dd36062da3df2f6380.patch', sha256='09c22e5c6fef327d3e48eb23f0d610dcd3a35ab9207f12e0f875701c677978d3', when='@2.6.4')
    patch('http://deb.debian.org/debian/pool/main/f/flex-old/flex-old_2.5.4a-10.diff.gz', archive_sha256='6a586c5c4d7c2d383e41c57a2a22edfa1deca12fcd5e838ede4c6651cde883b6', sha256='94d3ac948bb6a3f0b0a35fb67bf3704794511edeac0447477b60213d40a767fa', when='@2.5.4a-10')

    parallel = False

    @classmethod
    def determine_version(cls, exe):
        output = Executable(exe)('--version', output=str, error=str)
        match = re.search(r'flex\s+(\S+)', output)
        return match.group(1) if match else None

    @classmethod
    def determine_variants(cls, exes, version):
        results = []
        for exe in exes:
            variants = ''
            path = os.path.dirname(exe)
            if 'lex' in os.listdir(path):
                variants += "+lex"
            else:
                variants += "~lex"
            results.append(variants)
        return results

    @when('@2.5.39:2.6.0,2.6.4')
    def autoreconf(self, spec, prefix):
        autogen = Executable('./autogen.sh')
        autogen()

    @property
    def force_autoreconf(self):
        # The patch for 2.6.4 touches configure
        return self.spec.satisfies('@2.6.4')

    def url_for_version(self, version):
        url = "https://github.com/westes/flex"
        if version >= Version('2.6.1'):
            url += "/releases/download/v{0}/flex-{0}.tar.gz".format(version)
        elif version == Version('2.6.0'):
            url += "/archive/v{0}.tar.gz".format(version)
        elif version >= Version('2.5.37'):
            url += "/archive/flex-{0}.tar.gz".format(version)
        else:
            url += "/archive/flex-{0}.tar.gz".format(version.dashed)

        return url
    
    @run_before('configure')
    def copy_config_files(self):
        if self.spec.satisfies('@2.5.4a'):
            am = self.spec['automake'].prefix
            candidates = fs.find(am, files=["config.guess", "config.sub"], recursive=True)
            for name, good_files in itertools.groupby(
                    candidates, key=os.path.basename
            ):
                nf = next(good_files)
                fs.copy(nf, name)

    @run_after('install')
    def symlink_lex(self):
        """Install symlinks for lex compatibility."""
        if self.spec.satisfies('+lex'):
            dso = dso_suffix
            for dir, flex, lex in \
                    ((self.prefix.bin,   'flex', 'lex'),
                     (self.prefix.lib,   'libfl.a', 'libl.a'),
                     (self.prefix.lib,   'libfl.' + dso, 'libl.' + dso),
                     (self.prefix.lib64, 'libfl.a', 'libl.a'),
                     (self.prefix.lib64, 'libfl.' + dso, 'libl.' + dso)):

                if os.path.isdir(dir):
                    with working_dir(dir):
                        if (os.path.isfile(flex) and not os.path.lexists(lex)):
                            symlink(flex, lex)
