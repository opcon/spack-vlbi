#!/usr/bin/env bash

# run like:
#   mpi-wrapper-example <EXECUTABLE_NAME> <ARG 1> <ARG 2> ... <ARG N>

# enable spack
source "${SPACK_ROOT}/share/spack/setup-env.sh"

# activate difx environment
spack env activate difx

# save executable name (first argument)
executable=$1

# remove executable from argument list (shifting everything by 1)
shift 

# run executable with remaining arguments.
# we saved the executable name first, and so $* now only contains arguments AFTER the executable.
# this is equivalent to running <EXECUTABLE_NAME> <ARG 1> <ARG 2> ... <ARG N> when in the difx environment
${executable} $*
